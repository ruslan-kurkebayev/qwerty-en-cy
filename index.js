const localeEn = 'en',
      localeRu = 'rus',
      enRuMap = { 'A':'Ф','B':'И','C':'С','D':'В','E':'У','F':'А','G':'П','H':'Р','I':'Ш','J':'О','K':'Л','L':'Д','M':'Ь','N':'Т','O':'Щ','P':'З','Q':'Й','R':'К','S':'Ы','T':'Е',
                  'U':'Г','V':'М','W':'Ц','X':'Ч','Y':'Н','Z':'Я','<':'Б',',':'б','[':'х','{':'Х',']':'ъ','}':'Ъ',"'":'э','"':'Э','.':'ю','>':'Ю','/':'.','?':',','a':'ф','b':'и',
                  'c':'с','d':'в','e':'у','f':'а','g':'п','h':'р','i':'ш','j':'о','k':'л','l':'д','m':'ь','n':'т','o':'щ','p':'з','q':'й','r':'к','s':'ы','t':'е','u':'г','v':'м',
                  'w':'ц','x':'ч','y':'н','z':'я','&':'?','^':':','$':';','#':'№','@':'"','`':'ё','~':'Ё','|':'/',' ':' '
};

// Naively define the locale by the first found letter
function getLocale( text ){
  var locale = localeEn;  
  for ( l in text ) {
    if (/[A-Za-z]/.test( text[ l ] ) ) {
      break;
    }

    if (/[А-Яа-я]/.test(text[l])) {
      locale = localeRu;
      break;
    }
  }
  return locale;
}

// Translates text from Latin locale to Cyrillic locale
function cyrillicLocaleSwitcher( text ){
  var locale = getLocale( text );
  if ( locale === localeRu ) {
    return text;
  } else {
    var transletedText = '';
    for ( var i = 0; i < text.length; i ++ ){
      try{
        transletedText += enRuMap[ text[i] ];
      } catch ( e ){
        // Omit the letters we could not find
      }
    }
    return transletedText;
  }
};

module.exports = cyrillicLocaleSwitcher;